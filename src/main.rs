mod schema;

use isahc::HttpClient;
use log::{info, LevelFilter};

pub struct OctopusClient {
    inner: HttpClient,
    api_key: String,
}

impl OctopusClient {
    const BASE_URL: &'static str = "https://api.octopus.energy";

    pub fn new(api_key: String) -> anyhow::Result<Self> {
        Ok(Self {
            inner: HttpClient::new()?,
            api_key,
        })
    }
}

fn main() -> anyhow::Result<()> {
    pretty_env_logger::formatted_builder()
        .filter_level(LevelFilter::Info)
        .parse_default_env()
        .init();

    info!("tentacl time");
    Ok(())
}
