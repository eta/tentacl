// @generated automatically by Diesel CLI.

diesel::table! {
    configuration (id) {
        id -> Nullable<Integer>,
        octopus_api_key -> Text,
        octopus_account_id -> Text,
        mqtt_uri -> Nullable<Text>,
    }
}

diesel::table! {
    instantaneous_meter_readings (meter_id, time_unix, source) {
        meter_id -> Integer,
        reading -> Float,
        time_unix -> Integer,
        source -> Integer,
    }
}

diesel::table! {
    last_mqtt_reading (station_id) {
        station_id -> Nullable<Text>,
        electricity_total_value -> Float,
    }
}

diesel::table! {
    meter_readings (meter_id, interval_start_unix, source) {
        meter_id -> Integer,
        reading -> Float,
        interval_start_unix -> Integer,
        interval_end_unix -> Integer,
        source -> Integer,
    }
}

diesel::table! {
    meters (id) {
        id -> Nullable<Integer>,
        property_id -> Integer,
        mpan -> Text,
        serial_number -> Text,
        fuel_type -> Integer,
        is_usable -> Bool,
    }
}

diesel::table! {
    properties (octopus_id) {
        octopus_id -> Nullable<Integer>,
        move_in_ts -> Text,
        move_out_ts -> Nullable<Text>,
        address -> Text,
    }
}

diesel::table! {
    sync_state (id) {
        id -> Nullable<Integer>,
        last_properties_ts -> Nullable<Text>,
        last_tariffs_ts -> Nullable<Text>,
        last_consumption_ts -> Nullable<Text>,
    }
}

diesel::table! {
    tariff_rate (tariff_id, rate_type, valid_from_unix) {
        tariff_id -> Integer,
        rate_type -> Integer,
        value_inc_vat -> Float,
        valid_from_unix -> Integer,
        valid_to_unix -> Nullable<Integer>,
    }
}

diesel::table! {
    tariffs (id) {
        id -> Nullable<Integer>,
        property_id -> Integer,
        tariff_code -> Text,
        product_code -> Text,
        product_name_full -> Text,
        product_description -> Text,
        valid_from_ts -> Text,
        valid_to_ts -> Nullable<Text>,
    }
}

diesel::joinable!(instantaneous_meter_readings -> meters (meter_id));
diesel::joinable!(meter_readings -> meters (meter_id));
diesel::joinable!(meters -> properties (property_id));
diesel::joinable!(tariff_rate -> tariffs (tariff_id));
diesel::joinable!(tariffs -> properties (property_id));

diesel::allow_tables_to_appear_in_same_query!(
    configuration,
    instantaneous_meter_readings,
    last_mqtt_reading,
    meter_readings,
    meters,
    properties,
    sync_state,
    tariff_rate,
    tariffs,
);
