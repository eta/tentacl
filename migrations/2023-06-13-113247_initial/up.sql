PRAGMA foreign_keys = ON;

CREATE TABLE configuration (
    id INTEGER PRIMARY KEY,
    octopus_api_key TEXT NOT NULL,
    octopus_account_id TEXT NOT NULL,
    mqtt_uri TEXT
);

CREATE TABLE sync_state (
    id INTEGER PRIMARY KEY,
    last_properties_ts TEXT,
    last_tariffs_ts TEXT,
    last_consumption_ts TEXT
);

CREATE TABLE properties (
    octopus_id INTEGER PRIMARY KEY,
    move_in_ts TEXT NOT NULL,
    move_out_ts TEXT,
    address TEXT NOT NULL
);

CREATE TABLE meters (
    id INTEGER PRIMARY KEY,
    property_id INTEGER NOT NULL REFERENCES properties,
    mpan TEXT NOT NULL,
    serial_number TEXT NOT NULL,
    fuel_type INTEGER NOT NULL, -- Some enum for gas / elec, for future-proofing
    is_usable BOOLEAN NOT NULL, -- Set to false if the meter is non-smart
    UNIQUE(property_id, mpan, serial_number, fuel_type)
);

CREATE TABLE tariffs (
    id INTEGER PRIMARY KEY,
    property_id INTEGER NOT NULL REFERENCES properties,
    tariff_code TEXT NOT NULL,
    product_code TEXT NOT NULL,
    product_name_full TEXT NOT NULL,
    product_description TEXT NOT NULL,
    valid_from_ts TEXT NOT NULL,
    valid_to_ts TEXT
);

CREATE TABLE tariff_rate (
    tariff_id INTEGER NOT NULL REFERENCES tariffs,
    rate_type INTEGER NOT NULL, -- Some enum for, like, standing charge / elec unit / day unit / night unit, etc
    value_inc_vat REAL NOT NULL,
    valid_from_unix INTEGER NOT NULL,
    valid_to_unix INTEGER,
    PRIMARY KEY(tariff_id, rate_type, valid_from_unix)
);

CREATE TABLE meter_readings (
    meter_id INTEGER NOT NULL REFERENCES meters,
    reading REAL NOT NULL,
    interval_start_unix INTEGER NOT NULL,
    interval_end_unix INTEGER NOT NULL,
    source INTEGER NOT NULL, -- Some enum for like, Octopus versus MQTT
    PRIMARY KEY(meter_id, source, interval_start_unix)
);

CREATE TABLE last_mqtt_reading (
    station_id TEXT PRIMARY KEY,
    electricity_total_value REAL NOT NULL
);

CREATE TABLE instantaneous_meter_readings (
    meter_id INTEGER NOT NULL REFERENCES meters,
    reading REAL NOT NULL,
    time_unix INTEGER NOT NULL,
    source INTEGER NOT NULL,
    PRIMARY KEY(meter_id, source, time_unix)
);